package de.codelife.main;

import com.airhacks.afterburner.injection.Injector;
import de.codelife.presentation.application.ApplicationPresenter;
import de.codelife.presentation.application.ApplicationView;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        ApplicationView view = new ApplicationView();
        Scene scene = new Scene(view.getView());
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest((event) -> {System.exit(0);});
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        Injector.forgetAll();
    }

    public static void main(String... args){
        launch(args);
    }
}
