package de.codelife.presentation.login;

import de.codelife.business.service.Login;
import de.codelife.business.service.RestService;
import de.codelife.business.util.View;
import de.codelife.business.util.ViewManager;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import javax.inject.Inject;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.core.Response;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class LoginPresenter implements Initializable {

    @Inject RestService restService;

    @Inject ViewManager viewManager;

    @FXML TextField username;

    @FXML PasswordField password;

    @FXML Button btnLogin;

    @FXML BorderPane progressPane;

    @FXML AnchorPane loginPane;

    @FXML Label errorLabel;

    @FXML ComboBox<String> cbLogin;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cbLogin.setItems(FXCollections.observableArrayList(Arrays.asList("Task-Login", "InvocationCallback-Login", "UI Freeze-Login")));
    }

    @FXML
    private void validateInput(){
        btnLogin.setDisable(username.getText().isEmpty() || password.getText().isEmpty());
    }

    @FXML
    private void login(){
        progressPane.setVisible(true);
        loginPane.setDisable(true);
        errorLabel.setText("");
        switch (cbLogin.getSelectionModel().getSelectedItem()){
            case "Task-Login":
                taskLogin();
                break;
            case "InvocationCallback-Login":
                invocationCallbackLogin();
                break;
            case "UI Freeze-Login":
                uiFreezeLogin();
                break;
            default:
                break;

        }
    }

    private void uiFreezeLogin(){
        final Response loginResponse = restService.uiFreezeLogin(new Login(username.getText(), password.getText()));
        if(loginResponse != null){
            final String readEntity = loginResponse.readEntity(String.class);
            if(readEntity.contains("You have successfully logged in")){
                loginCompleted();
            } else {
                loginFailed(readEntity);
            }
        } else {
            loginFailed("Failed to login");
        }
    }

    private void taskLogin(){
        restService.taskLogin(response -> {
            if(response != null){
                final String readEntity = response.readEntity(String.class);
                if(readEntity.contains("You have successfully logged in")){
                    loginCompleted();
                } else {
                    loginFailed(readEntity);
                }
            } else {
                loginFailed("Failed to login");
            }

        },new Login(username.getText(), password.getText()));
    }

    private void invocationCallbackLogin(){
        restService.invocationCallbackLogin(new InvocationCallback<Response>() {
            @Override
            public void completed(Response response) {
                if(response != null){
                    String readEntity = response.readEntity(String.class);
                    if (readEntity.contains("You have successfully logged in")) {
                        loginCompletedPlatformRunLater();
                    } else {
                        loginFailedPlatformRunLater(readEntity);
                    }
                } else {
                    loginFailedPlatformRunLater("Failed to login");
                }
            }

            @Override
            public void failed(Throwable throwable) {
                loginFailedPlatformRunLater("Failed to login");
            }
        }, new Login(username.getText(), password.getText()));
    }


    private void loginCompleted(){
        progressPane.setVisible(false);
        loginPane.setDisable(false);
        viewManager.changeView(View.DASHBOARD_VIEW);
    }

    private void loginFailed(String msg){
        progressPane.setVisible(false);
        loginPane.setDisable(false);
        errorLabel.setText(msg);
    }

    //Die Folgenden zwei Methoden werden benötigt, um wieder zurück in den FX-Thread zu gelangen.
    //Dies ist für den InvocationCallbackLogin notwendig. Beim Task-Login wird dies durch die Klasse 'Task' für uns erledigt.

    private void loginCompletedPlatformRunLater(){
        Platform.runLater(() -> {
            loginCompleted();
        });
    }

    private void loginFailedPlatformRunLater(String msg){
        Platform.runLater(() -> {
            loginFailed(msg);
        });
    }
}
