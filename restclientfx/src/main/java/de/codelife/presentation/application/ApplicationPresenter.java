package de.codelife.presentation.application;

import de.codelife.business.util.IUpdateViewPresenter;
import de.codelife.business.util.View;
import de.codelife.business.util.ViewManager;
import de.codelife.presentation.dashboard.DashboardView;
import de.codelife.presentation.login.LoginView;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

public class ApplicationPresenter implements Initializable, IUpdateViewPresenter {

    @Inject LoginView loginView;

    @Inject DashboardView dashboardView;

    @Inject ViewManager viewManager;

    @FXML BorderPane rootPane;

    @FXML Button btnLogout;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        viewManager.registerPresenter(this);
        changeView(View.LOGIN_VIEW);
    }

    @FXML private void logout(){
        changeView(View.LOGIN_VIEW);
    }

    public void changeView(View view){
        switch (view){
            case LOGIN_VIEW:
                btnLogout.setVisible(false);
                rootPane.setCenter(loginView.getView());
                break;
            case DASHBOARD_VIEW:
                btnLogout.setVisible(true);
                rootPane.setCenter(dashboardView.getView());
                break;
            default:
                btnLogout.setVisible(false);
                rootPane.setCenter(loginView.getView());
                break;
        }
    }

    @Override
    public void update(View view) {
        changeView(view);
    }
}