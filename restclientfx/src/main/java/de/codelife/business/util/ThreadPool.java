package de.codelife.business.util;

import javafx.concurrent.Task;

import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ThreadPool<T>{
    
    private final Executor pool = Executors.newFixedThreadPool(2);

    public Executor executor() {
        return pool;
    }

    public void taskLogin(Callable<T> callable, Consumer<T> consumer){
        pool.execute(createTask(callable, consumer));
    }

    private Task<T> createTask(Callable<T> callable, Consumer<T> consumer){
        return new Task<T>() {
            @Override
            protected T call() throws Exception {
                return callable.call();
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                consumer.accept(getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                consumer.accept(getValue());
            }
        };
    }
}
