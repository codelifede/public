package de.codelife.business.util;

public interface IUpdateViewPresenter {

    void update(View view);

}
