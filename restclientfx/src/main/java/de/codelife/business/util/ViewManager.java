package de.codelife.business.util;

import java.util.HashSet;
import java.util.Set;

public class ViewManager  {

    private Set<IUpdateViewPresenter> presenters = new HashSet<>();

    public void registerPresenter(IUpdateViewPresenter presenter){
        presenters.add(presenter);
    }

    public void unregisterPresenter(IUpdateViewPresenter presenter){
        presenters.remove(presenter);
    }

    public void changeView(View view){
        this.presenters.forEach(presenter -> presenter.update(view));
    }
}
