package de.codelife.business.service;

import de.codelife.business.util.ThreadPool;
import org.glassfish.jersey.client.ClientProperties;

import javax.inject.Inject;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class RestService {

    @Inject ThreadPool threadPool;

    private final WebTarget target;

    public RestService(){
        final Client client = ClientBuilder.newClient();
        client.property(ClientProperties.CONNECT_TIMEOUT, 10000);
        client.property(ClientProperties.READ_TIMEOUT, 10000);
        this.target = client.target("http://localhost:8080/api/rest");

    }

    public void taskLogin(Consumer<Response> consumer, Login login){
        threadPool.taskLogin(createLoginCallable(login), consumer);
    }

    private Callable<?> createLoginCallable(Login login){
        return () -> {
            return target.path("/login")
                    .request(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .async()
                    .post(Entity.entity(login, MediaType.APPLICATION_JSON))
                    .get(10, TimeUnit.SECONDS); //Future get() call
        };
    }

    public void invocationCallbackLogin(InvocationCallback<Response> callback,Login login){
        threadPool.executor().execute(()-> createLoginFuture(callback, login));
    }

    private Future<Response> createLoginFuture(InvocationCallback<Response> callback, Login login){
        return target.path("/login")
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .async()
                .post(Entity.entity(login, MediaType.APPLICATION_JSON), callback);
    }

    public Response uiFreezeLogin(Login login){
        Response response = null;
        try{
            response = target.path("/login")
                        .request(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .post(Entity.entity(login, MediaType.APPLICATION_JSON));
        } catch (Exception e){
        }
        return response;
    }

}