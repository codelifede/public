package de.codelife.business.service;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Login {

    private String username;

    private String password;

    public Login(){super();}

    public Login(String username, String password){
        this.username = username;
        this.password = password;
    }

    @XmlElement
    public String getPassword() {
        return password;
    }
    @XmlElement
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
