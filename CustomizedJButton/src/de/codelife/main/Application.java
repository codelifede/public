package de.codelife.main;

import de.codelife.ui.CustomizedButtonUI;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Application {

    private static final JButton btnColor         = new JButton("codelife.de");
    private static final JButton btnColorWithFont = new JButton("codelife.de");
    private static final JButton btnIcon          = new JButton("codelife.de");
    private static final JButton btnIconWithFont  = new JButton("codelife.de");
    
    public static void main(String[] args) {
        setButtonUI();
        JFrame frame = new JFrame("Customized JButton Preview :: www.codelife.de");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(450, 300));
        frame.setLocationRelativeTo(null);
        frame.setLayout(new BorderLayout());
        frame.add(createPanel(),BorderLayout.CENTER);
        frame.setVisible(true);
    }
        

    private static JPanel createPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(2, 5, 2, 5);
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(new JLabel("Default JButton"), gbc);
        gbc.gridx = 1;
        panel.add(new JButton("codelife.de"), gbc);
        gbc.gridx = 0;
        gbc.gridy++;
        panel.add(new JLabel("JButton with customized colors"), gbc);
        gbc.gridx = 1;
        panel.add(btnColor, gbc);
        gbc.gridx = 0;
        gbc.gridy++;
        panel.add(new JLabel("JButton with customized colors and font"), gbc);
        gbc.gridx = 1;
        panel.add(btnColorWithFont, gbc);
        gbc.gridx = 0;
        gbc.gridy++;
        panel.add(new JLabel("JButton with customized icons"), gbc);
        gbc.gridx = 1;
        panel.add(btnIcon, gbc);
        gbc.gridx = 0;
        gbc.gridy++;
        panel.add(new JLabel("JButton with customized icons and font"), gbc);
        gbc.gridx = 1;
        panel.add(btnIconWithFont, gbc);
        return panel;
    }

    private static void setButtonUI(){
        btnColor.setUI(new CustomizedButtonUI(new Color(247,255,0), new Color(255,155,41),new Color(249,255,77)));
        
        btnColorWithFont.setUI(new CustomizedButtonUI(new Color(9,108,237), new Color(0,61,140), new Color(0,88,204), new Font("Tahmo", Font.BOLD, 14), Color.WHITE));
        
        btnIcon.setUI(new CustomizedButtonUI(loadImageIcon("btnIconNormal.png"), loadImageIcon("btnIconHover.png"), loadImageIcon("btnIconPressed.png")));
        btnIcon.setPreferredSize(new Dimension(145,35));
               
        btnIconWithFont.setUI(new CustomizedButtonUI(loadImageIcon("btnIconWithFontNormal.png"), loadImageIcon("btnIconWithFontHover.png"), loadImageIcon("btnIconWithFontPressed.png"), new Font("Tahmo", Font.BOLD, 14), Color.WHITE));
        btnIconWithFont.setPreferredSize(new Dimension(145,35));
    }
    
    private static ImageIcon loadImageIcon(String fileName){
        URL imgUrl = Application.class.getResource("../images/"+fileName);
        if(imgUrl != null){
            return new ImageIcon(imgUrl);
        }
        return null;
    }
}
