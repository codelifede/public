package de.codelife.server;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@SpringBootApplication
public class Application {

    public static  void main(String... args){
        SpringApplication.run(Application.class, args);
    }

}

@RestController
@RequestMapping(value = "/api/rest", produces = MediaType.APPLICATION_JSON_VALUE)
class LoginController {

    private String loginUsername = "admin";
    private String loginPassword = "admin123";

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@RequestBody Login login) {
        try {
            System.out.println(login);
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return login.getUsername().equalsIgnoreCase(loginUsername) && login.getPassword().equalsIgnoreCase(loginPassword)
                ? "You have successfully logged in"
                : "Wrong login credentials";
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
class Login {
    private String password;

    private String username;

    public Login(){
    }

    public String getPassword() {
        return password;
    }
    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return username+" "+password;
    }
}

