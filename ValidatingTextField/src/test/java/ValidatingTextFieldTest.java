import de.codelife.ValidatingTextField;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.util.WaitForAsyncUtils;

import static org.hamcrest.CoreMatchers.*;
import static org.testfx.api.FxAssert.*;

/**
 * © codelife.de
 */
public class ValidatingTextFieldTest extends ApplicationTest {

    private static final String REGEX = "[0-9]+";

    @Override
    public void start(Stage stage) throws Exception {
        ValidatingTextField notMandatoryValidationTextField = new ValidatingTextField(REGEX, false);
        notMandatoryValidationTextField.setId("notMandatoryValidationTextField");
        ValidatingTextField mandatoryValidationTextField = new ValidatingTextField(REGEX, true);
        mandatoryValidationTextField.setId("mandatoryValidationTextField");
        VBox box = new VBox();
        box.setSpacing(5);
        box.setPadding(new Insets(5,5,5,5));
        box.getChildren().addAll(notMandatoryValidationTextField, mandatoryValidationTextField);
        stage.setScene(new Scene(box));
        stage.show();
    }

    @Test
    public void validPropertyShouldBeTrueTest(){
        ValidatingTextField notMandatoryValidationTextField = findNotMandatoryValidationTextField();
        ValidatingTextField mandatoryValidationTextField = findMandatoryValidationTextField();
        clickOn(notMandatoryValidationTextField).write("0987654321");
        clickOn(mandatoryValidationTextField).write("0987654321");
        WaitForAsyncUtils.waitForFxEvents();
        verifyThat(notMandatoryValidationTextField.validProperty().getValue(), is(true));
        verifyThat(mandatoryValidationTextField.validProperty().getValue(), is(true));
    }

    @Test
    public void validPropertyShouldBeFalseTest(){
        ValidatingTextField notMandatoryValidationTextField = findNotMandatoryValidationTextField();
        ValidatingTextField mandatoryValidationTextField = findMandatoryValidationTextField();
        clickOn(notMandatoryValidationTextField).write("asdf");
        clickOn(mandatoryValidationTextField).write("asdf");
        WaitForAsyncUtils.waitForFxEvents();
        verifyThat(notMandatoryValidationTextField.validProperty().getValue(), is(false));
        verifyThat(mandatoryValidationTextField.validProperty().getValue(), is(false));
    }

    @Test
    public void validPropertyTest(){
        ValidatingTextField notMandatoryValidationTextField = findNotMandatoryValidationTextField();
        ValidatingTextField mandatoryValidationTextField = findMandatoryValidationTextField();
        clickOn(notMandatoryValidationTextField).write("0123").type(KeyCode.BACK_SPACE, 4);
        clickOn(mandatoryValidationTextField).write("0123").type(KeyCode.BACK_SPACE, 4);
        WaitForAsyncUtils.waitForFxEvents();
        verifyThat(mandatoryValidationTextField.validProperty().getValue(), is(false));
        verifyThat(notMandatoryValidationTextField.validProperty().getValue(), is(false));
    }

    @Test
    public void validPropertyShouldChangeValueTest(){
        ValidatingTextField notMandatoryValidationTextField = findNotMandatoryValidationTextField();
        ValidatingTextField mandatoryValidationTextField = findMandatoryValidationTextField();
        clickOn(notMandatoryValidationTextField).write("asdf");
        clickOn(mandatoryValidationTextField).write("asdf");
        WaitForAsyncUtils.waitForFxEvents();
        verifyThat(notMandatoryValidationTextField.validProperty().getValue(), is(false));
        verifyThat(mandatoryValidationTextField.validProperty().getValue(), is(false));
        clickOn(notMandatoryValidationTextField).type(KeyCode.BACK_SPACE,4).write("1234");
        clickOn(mandatoryValidationTextField).type(KeyCode.BACK_SPACE,4).write("1234");
        WaitForAsyncUtils.waitForFxEvents();
        verifyThat(notMandatoryValidationTextField.validProperty().getValue(), is(true));
        verifyThat(mandatoryValidationTextField.validProperty().getValue(), is(true));
    }

    @Test
    public void validationTextFieldChangesStyleClassToValidTest(){
        ValidatingTextField notMandatoryValidationTextField = findNotMandatoryValidationTextField();
        ValidatingTextField mandatoryValidationTextField = findMandatoryValidationTextField();
        clickOn(notMandatoryValidationTextField).write("1234567890");
        clickOn(mandatoryValidationTextField).write("1234567890");
        WaitForAsyncUtils.waitForFxEvents();
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_valid"), is(true));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_invalid"), is(false));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("mandatory"), is(false));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("is_valid"), is(true));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("is_invalid"), is(false));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("mandatory"), is(true));
    }

    @Test
    public void validationTextFieldChangesStyleClassToInvalidTest(){
        ValidatingTextField notMandatoryValidationTextField = findNotMandatoryValidationTextField();
        ValidatingTextField mandatoryValidationTextField = findMandatoryValidationTextField();
        clickOn(notMandatoryValidationTextField).write("qwert");
        clickOn(mandatoryValidationTextField).write("qwert");
        WaitForAsyncUtils.waitForFxEvents();
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_valid"), is(false));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_invalid"), is(true));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("mandatory"), is(false));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("is_valid"), is(false));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("is_invalid"), is(true));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("mandatory"), is(true));
    }


    @Test
    public void validationTextFieldChangesStyleClassFromValidToInvalidTest(){
        ValidatingTextField notMandatoryValidationTextField = findNotMandatoryValidationTextField();
        ValidatingTextField mandatoryValidationTextField = findMandatoryValidationTextField();
        clickOn(notMandatoryValidationTextField).write("12345");
        clickOn(mandatoryValidationTextField).write("12345");
        WaitForAsyncUtils.waitForFxEvents();
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_valid"), is(true));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_invalid"), is(false));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("mandatory"), is(false));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("is_valid"), is(true));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("is_invalid"), is(false));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("mandatory"), is(true));
        clickOn(notMandatoryValidationTextField).type(KeyCode.BACK_SPACE, 5);
        clickOn(mandatoryValidationTextField).type(KeyCode.BACK_SPACE, 5);
        WaitForAsyncUtils.waitForFxEvents();
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_valid"), is(false));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_invalid"), is(false));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("mandatory"), is(false));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("is_valid"), is(false));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("is_invalid"), is(false));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("mandatory"), is(true));
        clickOn(notMandatoryValidationTextField).write("qwert");
        clickOn(mandatoryValidationTextField).write("qwert");
        WaitForAsyncUtils.waitForFxEvents();
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_valid"), is(false));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_invalid"), is(true));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("mandatory"), is(false));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_valid"), is(false));
        verifyThat(notMandatoryValidationTextField.getStyleClass().contains("is_invalid"), is(true));
        verifyThat(mandatoryValidationTextField.getStyleClass().contains("mandatory"), is(true));
    }

    private ValidatingTextField findNotMandatoryValidationTextField(){
        return lookup("#notMandatoryValidationTextField").query();
    }
    private ValidatingTextField findMandatoryValidationTextField(){
        return lookup("#mandatoryValidationTextField").query();
    }
}
