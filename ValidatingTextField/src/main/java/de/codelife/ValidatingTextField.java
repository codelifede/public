package de.codelife;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

/**
 * Eine Erweitung des normalen {@link TextField} aus JavaFx.<br>
 * <p>
 * Es ermöglicht ungewollte Eingaben mit Hilfe einer Regex zu beschränken.<br>
 * Dem Nutzer wird durch automatische Änderung der Hintergrundfarbe eine direkte RÜckmeldung gegeben, ob seine Eingabe für dieses Feld valide ist oder nicht.
 * <br>
 * � codelife.de
 */
public class ValidatingTextField extends TextField {

    private final ReadOnlyObjectWrapper<Boolean> validProperty = new ReadOnlyObjectWrapper<>(false);
    private final SimpleObjectProperty<String> regexProperty = new SimpleObjectProperty<>("");
    private final SimpleObjectProperty<Boolean> mandatoryProperty = new SimpleObjectProperty<>(false);

    /**
     * Diese Methode wird von JavaFX/SceneBuilder benötigt.<br>
     * Alternativ gibt es noch folgende Konstruktoren:
     * <p>
     * {@link #ValidatingTextField(java.lang.String, boolean)}<br>
     * {@link #ValidatingTextField(java.lang.String, java.lang.String, boolean) }
     * </p>
     * <p>
     * Diese können benutzt werden, wenn man ein {@link ValidatingTextField} ohne FXML erzeugen möchte.<br>
     * </p>
     *
     */
    public ValidatingTextField(){
        this("", false);
    }

    /**
     * @param regex Regex für die Validierung
     * @param mandatory Ob es sich um ein Pflichfeld handelt
     */
    public ValidatingTextField(String regex, boolean mandatory){
        this("", regex, mandatory);
    }

    /**
     * @param text Text der in das TextField direkt eingetragen werden soll
     * @param regex Regex für die Validierung
     * @param mandatory Ob es sich um ein Pflichfeld handelt
     */
    public ValidatingTextField(String text, String regex, boolean mandatory){
        super(text);
        this.getStylesheets().add("de/codelife/validatingTextField.css");
        this.mandatoryProperty.addListener(this::onMandatoryPropertyChanged);
        this.textProperty().addListener(this::onTextPropertyChanged);
        this.regexProperty.setValue(regex);
        this.mandatoryProperty.setValue(mandatory);
    }

    private void onMandatoryPropertyChanged(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue){
        if(newValue){
            if(!this.getStyleClass().contains("mandatory")){
                this.getStyleClass().add("mandatory");
            }
        } else {
            if(this.getStyleClass().contains("mandatory")){
                this.getStyleClass().remove("mandatory");
            }
        }
    }

    private void onTextPropertyChanged(ObservableValue<? extends String> observable, String oldValue, String newValue){
        if(newValue != null){
            validProperty.setValue(newValue.matches(regexProperty.getValue()));
            if(!newValue.isEmpty()) {
                changeStyle();
            } else {
                this.getStyleClass().removeAll("is_valid","is_invalid");
            }
        }
    }

    private void changeStyle(){
        if(validProperty.getValue()){
            if(!this.getStyleClass().contains("is_valid")){
                this.getStyleClass().remove("is_invalid");
                this.getStyleClass().add("is_valid");
            }
        } else {
            if(!this.getStyleClass().contains("is_invalid")){
                this.getStyleClass().remove("is_valid");
                this.getStyleClass().add("is_invalid");
            }
        }
    }

    /**
     * @return Ob die Eingaben valide sind
     */
    public final ReadOnlyObjectProperty<Boolean> validProperty(){
        return validProperty.getReadOnlyProperty();
    }

    /**
     * @return Die RegexProperty für dieses {@link ValidatingTextField}
     */
    public final ObjectProperty<String> regexProperty(){
        return regexProperty;
    }

    /**
     * @return Die Regex als String
     */
    public String getRegex(){
        return regexProperty.getValue();
    }

    /**
     * @param value Die Regex als String
     */
    public void setRegex(String value){
        regexProperty.setValue(value);
    }

    /**
     * @return Ob das {@link ValidatingTextField} ein Pflichtfeld ist.
     */
    public final ObjectProperty<Boolean> mandatoryProperty(){
        return mandatoryProperty;
    }

    /**
     * @return Ob das {@link ValidatingTextField} ein Pflichtfeld ist.
     */
    public boolean isMandatory(){
        return mandatoryProperty.getValue();
    }

    /**
     * @param value Ob das {@link ValidatingTextField} ein Pflichtfeld sein soll.
     */
    public void setMandatory(boolean value){
        mandatoryProperty.setValue(value);
    }
}
