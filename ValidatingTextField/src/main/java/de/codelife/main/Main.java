package de.codelife.main;

import de.codelife.ValidatingTextField;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 */
public class Main extends Application {

    public static void main(String... args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        ValidatingTextField validatingTextField1 = new ValidatingTextField("[0-9]+",true);
        ValidatingTextField validatingTextField2 = new ValidatingTextField("[0-9]+", true);
        ValidatingTextField validatingTextField3 = new ValidatingTextField("[0-9]+", true);

        ValidatingTextField validatingTextField4 = new ValidatingTextField("[a-zA-Z]+", false);
        ValidatingTextField validatingTextField5 = new ValidatingTextField("[a-zA-Z]+", false);
        ValidatingTextField validatingTextField6 = new ValidatingTextField("[a-zA-Z]+", false);

        GridPane grid = new GridPane();
        grid.setStyle("-fx-background-color: white;");
        grid.setPadding(new Insets(10,10,10,10));
        grid.setVgap(5);
        grid.add(new Label("mandatory and empty: "), 0,0);
        grid.add(validatingTextField1, 1,0);
        grid.add(new Label("mandatory and valid: "), 0,1);
        grid.add(validatingTextField2, 1,1);
        grid.add(new Label("mandatory and invalid: "), 0,2);
        grid.add(validatingTextField3, 1,2);
        grid.add(new Label("not mandatory and empty: "), 0,3);
        grid.add(validatingTextField4, 1 ,3);
        grid.add(new Label("not mandatory and valid: "), 0,4);
        grid.add(validatingTextField5, 1,4);
        grid.add(new Label("not mandatory and invalid: "), 0,5);
        grid.add(validatingTextField6, 1,5);

        primaryStage.setScene(new Scene(grid));
        primaryStage.setTitle("ValidatingTextField Example :: codelife.de");
        primaryStage.show();
    }
}
